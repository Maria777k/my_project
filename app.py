from flask import Flask, render_template, request, redirect
import sqlite3

app = Flask(__name__)


@app.route("/")
def index():
    with sqlite3.connect("base.db") as connection:
        cr = connection.cursor()
        cr.execute("SELECT * FROM base")
        tracks = cr.fetchall()
    return render_template("index.html", tracks=tracks)

@app.route("/add_track/", methods=["GET", "POST"])
def add_track():
    if request.method == "POST":
        with sqlite3.connect("base.db") as connection:
            query = f'INSERT INTO tracks (track, singer, time) VALUES ("{request.form["track"]}", "{request.form["singer"]}", f"{request.form["time"]}")'
            cursor = connection.cursor()
            cursor.execute(query)
            connection.commit()
            return redirect("/")
    return render_template("add_track.html")

@app.route("/delete_track/", methods=["GET", "POST"])
def delete_track():
    if request.method == "POST":
        with sqlite3.connect("base.db") as connection:
            cursor = connection.cursor()
            cursor.execute(f"DELETE FROM track WHERE id={request.form['track_id']}")
            connection.commit()
    return redirect("/")

if __name__ == "__main__":
    app.run(debug=True)